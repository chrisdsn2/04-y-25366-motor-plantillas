const express = require("express");
const router = express.Router();
const {getAll} = require("../db/conexion");

//Como prefijo /admin
router.get("/", (req, res) => {
    res.render("admin/index");
});

//Como prefijo /admin/integrantes/listar
router.get("/integrantes/listar", async (req, res) => {
    const sql = "SELECT * FROM integrantes WHERE activo = 1";
    console.log("Buscando integrantes...");// Si hay error muestra en consola

    try {
        const integrantes = await getAll(sql);
       // console.log("Integrantes obtenido con éxito:", integrantes); // Si hay error muestra en consola

        res.render("admin/integrantes/index", {
            integrantes: integrantes,
        });
    } catch (err) {
        console.error("Error fetching integrantes:", err.message);
        res.status(500).send("Error Interno del Servidor");
    }
});

//Como prefijo /admin/tipo_media/listar
router.get("/tipo_media/listar", async (req, res) => {
    const sqltm = "SELECT * FROM tipoMedia WHERE activo = 1";
    console.log("Buscando Tipo Media...");// Si hay error muestra en consola

    try {
        const tipoMedia = await getAll(sqltm);
       console.log("Tipo Media obtenido con éxito:", tipoMedia); // Si hay error muestra en consola

        res.render("admin/tipo_media/index", {
            tipoMedia: tipoMedia,
        });
    } catch (err) {
        console.error("Error buscando Tipo Media:", err.message);
        res.status(500).send("Error Interno del Servidor");
    }
});

//Como prefijo /admin/media/listar
router.get("/media/listar", async (req, res) => {
    const sqltm = "SELECT * FROM media WHERE activo = 1";
    console.log("Buscando Media...");// Si hay error muestra en consola

    try {
        const media = await getAll(sqltm);
       console.log("Tipo Media obtenido con éxito:", media); // Si hay error muestra en consola

        res.render("admin/media/index", {
            media: media,
        });
    } catch (err) {
        console.error("Error buscando media:", err.message);
        res.status(500).send("Error Interno del Servidor");
    }
});



router.get("/integrantes/crear", async (req, res) => {
    res.render("admin/integrantes/crearForm")
});

router.post("/integrantes/create", async (req, res) => {
    
});


//exportar
module.exports = router;