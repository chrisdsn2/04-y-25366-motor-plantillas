// importar express
const express = require("express");
const hbs = require("hbs");

require('dotenv').config(); //nav.example
//requerimos la conexion a la base de datos
const db = require("./db/conexion");
//creamos la aplicacion express
const app = express();
const path = require('path');

//importamos archivos de rutas
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');//para los tipo admin

//Incluir la ruta dentro del proyecto express
app.use("/", router);
app.use("/admin", routerAdmin);//llama para los tipo administracion

app.use(express.static('public'));
app.set('view engine', 'hbs');

// se configura que se apunta a la carpeta views
app.set("views", __dirname + "/views");

//para el navegacion generica
hbs.registerPartials(__dirname + "/views/partials");

//
// Middleware para manejo de errores
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

// iniciar app escuchando puerto parametro
const puerto = process.env.PORT ||3000;

app.listen(puerto, () => {
    console.log("Servidor corriendo en el puerto"+puerto);
});

//Se utiliza consola para que se imprima una notificacion
//console.log("base de datos simulada", db);
//console.log(db.integrantes);
//console.log(db.integrantes[0].codigo);